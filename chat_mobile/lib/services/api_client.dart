import 'package:chat_api_client/chat_api_client.dart';
import 'package:flutter/foundation.dart';

import '../entities/app_state.dart';

class MobileApiClient extends ApiClient {
  MobileApiClient({@required AppState appState})
      : super(Uri.parse(appState.chatApiAddress),
            onBeforeRequest: (ApiRequest request) {
          if (appState.authToken != null) {
            return request.change(
                headers: {}
                  ..addAll(request.headers)
                  ..addAll({'authorization': appState.authToken}));
          }
          return request;
        }, onAfterResponse: (ApiResponse response) {
          if (response.headers.containsKey('authorization')) {
            appState.authToken = response.headers['authorization'];
          }
          return response;
        });
}
