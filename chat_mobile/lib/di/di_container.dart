import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';

import '../bloc_common/bloc_provider.dart';
import '../data/i_data_provider.dart';
import '../data/repository.dart';
import '../data/web_socket_data_provider.dart';
import '../entities/app_state.dart';
import '../features/chat_content/chat_content_bloc.dart';
import '../features/chat_content/chat_content_screen.dart';
import '../features/chat_list/chat_list_bloc.dart';
import '../features/chat_list/chat_list_screen.dart';
import '../features/login/login_bloc.dart';
import '../features/login/login_screen.dart';
import '../features/startup/startup_bloc.dart';
import '../features/startup/startup_screen.dart';
import 'screen_builder.dart';

class DiContainer {
  static Injector _injector;

  static void initialize() {
    _injector = Injector.getInjector();
    _registerServices();
    _registerScreenBuilders();
  }

  static Widget getStartupScreen() {
    return (_injector.get<StartupScreenBuilder>())();
  }

  static void _registerServices() {
    _injector.map<ScreenBuilder>((i) => ScreenBuilder(injector: i),
        isSingleton: true);

    _injector.map<AppState>((i) => AppState(), isSingleton: true);

    _injector.map<IDataProvider>(
        (i) => WebSocketDataProvider(appState: i.get<AppState>()),
        isSingleton: true);

    _injector.map<Repository>(
        (i) => Repository(
            appState: i.get<AppState>(), dataProvider: i.get<IDataProvider>()),
        isSingleton: true);
  }

  static void _registerScreenBuilders() {
    // Startup screen
    _injector.map<StartupScreenBuilder>(
        (i) => () => BlocProvider<StartupBloc>(
              child: StartupScreen(),
              bloc: StartupBloc(
                  appState: i.get<AppState>(),
                  repository: i.get<Repository>(),
                  screenBuilder: i.get<ScreenBuilder>()),
            ),
        isSingleton: true);

    // Login screen
    _injector.map<LoginScreenBuilder>(
        (i) => () => BlocProvider<LoginBloc>(
              child: LoginScreen(),
              bloc: LoginBloc(
                  appState: i.get<AppState>(),
                  repository: i.get<Repository>(),
                  screenBuilder: i.get<ScreenBuilder>()),
            ),
        isSingleton: true);

    // Chat list screen
    _injector.map<ChatListScreenBuilder>(
        (i) => () => BlocProvider<ChatListBloc>(
              child: ChatListScreen(),
              bloc: ChatListBloc(
                  appState: i.get<AppState>(),
                  repository: i.get<Repository>(),
                  screenBuilder: i.get<ScreenBuilder>()),
            ),
        isSingleton: true);

    // Chat content screen
    _injector.map<ChatContentScreenBuilder>(
        (i) => () => BlocProvider<ChatContentBloc>(
              child: ChatContentScreen(),
              bloc: ChatContentBloc(
                  appState: i.get<AppState>(),
                  repository: i.get<Repository>(),
                  screenBuilder: i.get<ScreenBuilder>()),
            ),
        isSingleton: true);
  }
}
