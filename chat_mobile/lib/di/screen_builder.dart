import 'package:flutter/material.dart';
import 'package:flutter_simple_dependency_injection/injector.dart';

import '../bloc_common/bloc_provider.dart';
import '../features/chat_content/chat_content_bloc.dart';
import '../features/chat_list/chat_list_bloc.dart';
import '../features/login/login_bloc.dart';
import '../features/startup/startup_bloc.dart';

typedef StartupScreenBuilder = BlocProvider<StartupBloc> Function();
typedef LoginScreenBuilder = BlocProvider<LoginBloc> Function();
typedef ChatListScreenBuilder = BlocProvider<ChatListBloc> Function();
typedef ChatContentScreenBuilder = BlocProvider<ChatContentBloc> Function();

class ScreenBuilder {
  ScreenBuilder({@required Injector injector}) : _injector = injector;

  final Injector _injector;

  Widget Function() getLoginScreenBuilder() =>
      _injector.get<LoginScreenBuilder>();

  Widget Function() getChatListScreenBuilder() =>
      _injector.get<ChatListScreenBuilder>();

  Widget Function() getChatContentScreenBuilder() =>
      _injector.get<ChatContentScreenBuilder>();
}
