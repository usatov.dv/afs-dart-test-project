import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:chat_models/chat_models.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

import '../entities/app_state.dart';
import 'i_data_provider.dart';

class Repository {
  Repository(
      {@required AppState appState, @required IDataProvider dataProvider})
      : _appState = appState,
        _dataProvider = dataProvider {
    isInitComplete = _initialize();
    _log.i('create');
  }

  final AppState _appState;
  final IDataProvider _dataProvider;
  Future<bool> isInitComplete;
  final FimberLog _log = FimberLog('Repository');

  // new message stream from DataProvider
  Stream<String> _dataStream;

  final _unreadChatIds = HashSet<ChatId>();
  final _unreadChatIdsStreamController = BehaviorSubject<Set<ChatId>>();
  Stream<Set<ChatId>> unreadChatIdsStream;

  var _chats = <Chat>[];
  final _chatsStreamController = BehaviorSubject<List<Chat>>();
  Stream<List<Chat>> chatsStream;

  var _users = <User>[];
  final _usersStreamController = BehaviorSubject<List<User>>();
  Stream<List<User>> usersStream;

  var _messages = <Message>[];
  final _messagesStreamController = BehaviorSubject<List<Message>>();
  Stream<List<Message>> messagesStream;

  final _notificationStreamController = PublishSubject<String>();
  Stream<String> notificationStream;

  Future<bool> _initialize() async {
    _log.d('initialize() start');

    unreadChatIdsStream = _unreadChatIdsStreamController.stream;
    chatsStream = _chatsStreamController.stream;
    usersStream = _usersStreamController.stream;
    messagesStream = _messagesStreamController.stream;
    notificationStream = _notificationStreamController.stream;

    _dataStream = await _dataProvider.initialize();
    _dataStream.listen(_handleDataFromProvider);

    _log.d('initialize() end');
    return true;
  }

  void _handleDataFromProvider(String data) {
    Message receivedMessage;
    try {
      receivedMessage = Message.fromJson(json.decode(data));
    } on Exception catch (err) {
      _log.e('Deserialize message failed', ex: err);
    }

    if (receivedMessage != null) {
      final chatId = receivedMessage.chat;

      if (_appState.currentChat?.id == chatId) {
        // if we are reading this chat
        _messages.add(receivedMessage);
        refreshMessages();
      } else {
        // collect chatId with unread message
        addUnreadChat(chatId: chatId);
        refreshChats();
        // send notification about new message
        _notificationStreamController
            .add('${receivedMessage.author.name}: ${receivedMessage.text}');
      }
    }
  }

  Future<User> loginUser(String login, String password) {
    return _dataProvider.loginUser(login, password);
  }

  Future<bool> signupUser(String login, String password) {
    return _dataProvider.signupUser(login, password);
  }

  Future<void> getChats() async {
    var found = await _dataProvider.getChats();
    if (found != null) {
      _chats = found;
      refreshChats();
    }
  }

  Future<void> getUsers() async {
    var found = await _dataProvider.getUsers();
    if (found != null) {
      _users = found;
      refreshUsers();
    }
  }

  Future<void> getMessages() async {
    var found = await _dataProvider.getMessages();
    if (found != null) {
      _messages = found;
      refreshMessages();
    }
  }

  void refreshChats() {
    _chatsStreamController.add(_chats);
  }

  void refreshUsers() {
    _usersStreamController.add(_users);
  }

  void refreshMessages() {
    _messagesStreamController.add(_messages);
  }

  void addUnreadChat({ChatId chatId}) {
    _unreadChatIds.add(chatId);
    _unreadChatIdsStreamController.add(_unreadChatIds);
  }

  void removeUnreadChat({ChatId chatId}) {
    _unreadChatIds.remove(chatId);
    _unreadChatIdsStreamController.add(_unreadChatIds);
  }

  Future<Chat> createChat({List<UserId> memberIds}) async {
    var members = _users.where((u) => memberIds.contains(u.id)).toList();
    members.add(_appState.currentUser);
    var newChat = await _dataProvider.createChat(members: members);
    if (newChat != null) {
      getChats();
    }
    return newChat;
  }

  Future<bool> sendMessage({Message message}) async {
    var result = await _dataProvider.sendMessage(message: message);
    // test back message
    sendBackMessage(inMessage: message);
    return result;
  }

  Future<void> sendBackMessage({Message inMessage}) async {
    // var author = _appState.currentChat.members
    //     .firstWhere((user) => user.id != _appState.currentUser.id);
    var delay = Duration(seconds: 5);
    var backMessage = Message(
        chat: inMessage.chat,
        author: inMessage.author,
        text: 'И тебе ${inMessage.text}',
        createdAt: DateTime.now().add(delay));
    Future.delayed(delay, () {
      _dataProvider.sendMessage(message: backMessage);
    });
  }

  void clearChatContent() {
    _appState.currentChat = null;
    _messages = [];
    refreshMessages();
  }

  void dispose() {
    _unreadChatIdsStreamController.close();
    _chatsStreamController.close();
    _usersStreamController.close();
    _messagesStreamController.close();
    _notificationStreamController.close();
  }
}
