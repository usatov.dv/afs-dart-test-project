import 'dart:async';
import 'dart:io';

import 'package:chat_api_client/chat_api_client.dart';
import 'package:chat_models/chat_models.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/foundation.dart';

import '../entities/app_state.dart';
import '../services/api_client.dart';
import 'i_data_provider.dart';

class WebSocketDataProvider extends IDataProvider {
  WebSocketDataProvider({@required AppState appState})
      : _appState = appState,
        _address = appState.webSocketAddress;

  final AppState _appState;
  final String _address;
  WebSocket _webSocket;
  StreamSubscription _wsStreamSubscription;
  final StreamController<String> dataStreamController =
      StreamController<String>();
  final FimberLog _log = FimberLog('WebSocketDataProvider');

  @override
  Future<Stream<String>> initialize() async {
    _log.d('initialize() start');

    await connect();

    _log.d('initialize() end');
    return dataStreamController.stream;
  }

  Future<void> connect() async {
    var ws = WebSocket.connect(_address);
    ws.then((webSocket) {
      _webSocket = webSocket;
      _wsStreamSubscription = webSocket.listen((data) {
        if (data is String) {
          dataStreamController.add(data);
        }
      })
        ..onError((err) {
          // simple websocket reconnect policy
          _log.e(err);
          connect();
        });
    });
  }

  @override
  Future<User> loginUser(String login, String password) async {
    User user;
    try {
      var usersClient = UsersClient(MobileApiClient(appState: _appState));
      user = await usersClient.login(login, password);
    } on Exception catch (err) {
      _log.e('Login failed', ex: err);
    }
    return user;
  }

  @override
  Future<bool> signupUser(String login, String password) async {
    var result = false;
    try {
      var usersClient = UsersClient(MobileApiClient(appState: _appState));
      await usersClient.create(User(name: login, password: password));
      result = true;
    } on Exception catch (err) {
      _log.e('Sign up failed', ex: err);
    }
    return result;
  }

  @override
  Future<List<Chat>> getChats() async {
    List<Chat> found;
    try {
      var chatsClient = ChatsClient(MobileApiClient(appState: _appState));
      found = await chatsClient.read({});
    } on Exception catch (err) {
      _log.e('Failed to get list of chats', ex: err);
    }
    return found;
  }

  @override
  Future<List<User>> getUsers() async {
    List<User> found;
    try {
      var usersClient = UsersClient(MobileApiClient(appState: _appState));
      found = await usersClient.read({});
      found.removeWhere((user) => user.id == _appState.currentUser.id);
    } on Exception catch (err) {
      _log.e('Failed to get list of users', ex: err);
    }
    return found;
  }

  @override
  Future<List<Message>> getMessages() async {
    List<Message> found;
    try {
      found = await MessagesClient(MobileApiClient(appState: _appState))
          .read(_appState.currentChat.id);
    } on Exception catch (err) {
      _log.e('Failed to get list of messages', ex: err);
    }
    return found;
  }

  @override
  Future<Chat> createChat({List<User> members}) async {
    Chat createdChat;
    if (members.isNotEmpty) {
      try {
        var chatsClient = ChatsClient(MobileApiClient(appState: _appState));
        createdChat = await chatsClient.create(Chat(members: members));
      } on Exception catch (err) {
        _log.e('Chat creation failed', ex: err);
      }
    }
    return createdChat;
  }

  @override
  Future<bool> sendMessage({Message message}) async {
    var result = false;
    try {
      await MessagesClient(MobileApiClient(appState: _appState))
          .create(message);
      result = true;
    } on Exception catch (err) {
      _log.e('Sending message failed', ex: err);
      print(err);
    }
    return result;
  }

  @override
  void dispose() {
    dataStreamController.close();
    _wsStreamSubscription?.cancel()?.then((_) {
      _webSocket?.close();
    });
    _log.i('dispose()');
  }
}
