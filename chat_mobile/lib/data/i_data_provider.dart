import 'dart:async';

import 'package:chat_models/chat_models.dart';

abstract class IDataProvider {
  Future<Stream<String>> initialize();

  Future<User> loginUser(String login, String password);

  Future<bool> signupUser(String login, String password);

  Future<List<Chat>> getChats();

  Future<List<User>> getUsers();

  Future<List<Message>> getMessages();

  Future<Chat> createChat({List<User> members});

  Future<bool> sendMessage({Message message});

  void dispose();






}
