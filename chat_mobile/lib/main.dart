import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';

import 'di/di_container.dart';

void main() {
  Fimber.plantTree(DebugTree()); // init logger
  DiContainer.initialize(); // init dependency injector
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Simple Chat',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: DiContainer.getStartupScreen(),
    );
  }
}
