import 'package:chat_models/chat_models.dart';

class AppState {
  AppState({this.authToken, this.currentUser}) {
    host = '10.0.2.2';
    webSocketAddress = 'ws://$host:3333/ws';
    chatApiAddress = 'http://$host:3333';
  }

  String host;
  String webSocketAddress;
  String chatApiAddress;

  String authToken;
  User currentUser;

  Chat currentChat;
  int tabBarIndex;
  int tabUsersScrollIndex;
  int tabChatsScrollIndex;
}
