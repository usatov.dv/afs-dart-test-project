import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';

import '../../bloc_common/i_bloc.dart';
import '../../data/repository.dart';
import '../../di/screen_builder.dart';
import '../../entities/app_state.dart';

class LoginBloc extends IBloc {
  LoginBloc(
      {@required AppState appState,
      @required Repository repository,
      @required ScreenBuilder screenBuilder})
      : _appState = appState,
        _repository = repository,
        _screenBuilder = screenBuilder {
    _log.i('create');
  }

  final AppState _appState;
  final Repository _repository;
  final ScreenBuilder _screenBuilder;
  String login = '';
  String password = '';
  BuildContext context;
  final _log = FimberLog('LoginBloc');

  String validateLogin(String value) {
    if (value.length < 2) {
      // check login rules here
      return 'The Login must be at least 2 characters.';
    }
    return null;
  }

  String validatePassword(String value) {
    if (value.length < 2) {
      // check password rules here
      return 'The Password must be at least 2 characters.';
    }
    return null;
  }

  void loginUser(BuildContext context) async {
    var user = await _repository.loginUser(login, password);

    if (user != null) {
      _appState.currentUser = user;
      var nextScreen = _screenBuilder.getChatListScreenBuilder();
      Navigator.pushReplacement<Widget, Widget>(context,
          MaterialPageRoute<Widget>(builder: (context) => nextScreen()));
    } else {
      var snackBar = SnackBar(
          content: Text('Login failed'), duration: Duration(seconds: 1));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  void signupUser(BuildContext context) async {
    var result = await _repository.signupUser(login, password);
    if (result) {
      loginUser(context);
    } else {
      var snackBar = SnackBar(
          content: Text('Sign up failed'), duration: Duration(seconds: 1));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }

  @override
  void dispose() {
    _log.i('dispose');
  }
}
