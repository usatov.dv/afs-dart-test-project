import 'package:flutter/material.dart';

import '../../bloc_common/bloc_provider.dart';
import 'login_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _bloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _loginController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of(context);
    _bloc.context = context;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Builder(
      builder: (scaffoldContext) {
        return Container(
          padding: EdgeInsets.all(20.0),
          child: Center(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: _loginController,
                    validator: _bloc.validateLogin,
                    onSaved: (String value) {
                      _bloc.login = value;
                    },
                    decoration: InputDecoration(
                        hintText: 'Login', labelText: 'Enter your login'),
                  ),
                  TextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    // Use secure text for passwords.
                    validator: _bloc.validatePassword,
                    onSaved: (String value) {
                      _bloc.password = value;
                    },
                    decoration: InputDecoration(
                        hintText: 'Password', labelText: 'Enter your password'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        RaisedButton(
                            child: Text("Login"),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                _bloc.loginUser(scaffoldContext);
                              }
                            }),
                        FlatButton(
                          child: Text("Sign up"),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              _showDialog('Create user ${_bloc.login} ?')
                                  .then((result) {
                                if (result != null &&
                                    result is bool &&
                                    result) {
                                  _bloc.signupUser(scaffoldContext);
                                }
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    ));
  }

  Future<bool> _showDialog(String questionText) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Text(questionText),
          actions: <Widget>[
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: Text("Ok"),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
  }
}
