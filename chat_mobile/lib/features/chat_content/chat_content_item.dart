import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'chat_content_bloc.dart';

class ChatContentItem extends StatelessWidget {
  ChatContentItem(this.item, this.bloc);

  final Message item;
  final ChatContentBloc bloc;
  final TextStyle headerTextStyle =
      TextStyle(fontWeight: FontWeight.bold, fontSize: 14);

  @override
  Widget build(BuildContext context) {
    var isMyMessage = item.author.id == bloc.currentUser.id;

    return Container(
      alignment: isMyMessage ? Alignment.centerRight : Alignment.centerLeft,
      padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
      child: FractionallySizedBox(
        widthFactor: 0.9,
        child: GestureDetector(
          onTap: () {},
          child: Container(
            decoration: BoxDecoration(
                color: isMyMessage ? Colors.green[50] : Colors.yellow[50],
                border: Border.all(),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                    bottomLeft:
                        isMyMessage ? Radius.circular(8) : Radius.circular(0),
                    bottomRight:
                        isMyMessage ? Radius.circular(0) : Radius.circular(8))),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          spacing: 8,
                          alignment: WrapAlignment.start,
                          children: [
                            Text(item.author.name, style: headerTextStyle)
                          ],
                        ),
                      ),
                      Text(DateFormat('HH:mm').format(item.createdAt),
//                            overflow: TextOverflow.ellipsis,
                          style: headerTextStyle),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(item.text, style: TextStyle(fontSize: 14)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
