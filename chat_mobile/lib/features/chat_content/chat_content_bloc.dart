import 'dart:async';

import 'package:chat_models/chat_models.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';

import '../../bloc_common/i_bloc.dart';
import '../../data/repository.dart';
import '../../di/screen_builder.dart';
import '../../entities/app_state.dart';

class ChatContentBloc extends IBloc {
  ChatContentBloc(
      {@required AppState appState,
      @required Repository repository,
      @required ScreenBuilder screenBuilder})
      : _appState = appState,
        _repository = repository,
        _screenBuilder = screenBuilder {
    initialize();
    _log.i('create');
  }

  final AppState _appState;
  final Repository _repository;
  final ScreenBuilder _screenBuilder;
  Stream<List<Message>> messagesStream;
  Stream<Set<ChatId>> _unreadChatIdsStream;
  Set<ChatId> unreadChatIds = {};
  Chat currentChat;
  User currentUser;
  StreamController<bool> needRefreshUI = StreamController<bool>();
  BuildContext context;
  final _log = FimberLog('ChatContentBloc');

  void initialize() async {
    currentChat = _appState.currentChat;
    currentUser = _appState.currentUser;
    messagesStream = _repository.messagesStream;
    _repository.removeUnreadChat(chatId: currentChat.id);

    _unreadChatIdsStream = _repository.unreadChatIdsStream;
    _unreadChatIdsStream.listen((chatIds) {
      unreadChatIds.clear();
      unreadChatIds.addAll(chatIds);
      needRefreshUI.sink.add(true);
    });

    refreshChatContent();
  }

  Future<void> refreshChatContent() async {
    await _repository.getMessages();
  }

  void sendMessage(String text) {
    final newMessage = Message(
        chat: currentChat.id,
        author: currentUser,
        text: text,
        createdAt: DateTime.now());
    _repository.sendMessage(message: newMessage);
  }

  void onTapNewMessageNotification() {
    Navigator.pop(context);
  }

  @override
  void dispose() {
    _repository.clearChatContent();
    needRefreshUI.close();
    _log.i('dispose');
  }
}
