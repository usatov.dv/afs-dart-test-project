import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

import '../../bloc_common/bloc_provider.dart';
import 'chat_content_bloc.dart';
import 'chat_content_item.dart';

class ChatContentScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChatContentScreenState();
}

class _ChatContentScreenState extends State<ChatContentScreen> {
  ChatContentBloc _bloc;
  final _sendMessageTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of(context);
    _bloc.context = context;
    _bloc.needRefreshUI.stream.listen((event) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final messagesList = StreamBuilder(
      stream: _bloc.messagesStream,
      builder: (BuildContext context, AsyncSnapshot<List<Message>> snapshot) {
        if (snapshot.data == null) {
          return Center(
              child: CircularProgressIndicator(
            strokeWidth: 2.0,
          ));
        } else if (snapshot.data.isEmpty) {
          return Center(
              child: Text(
            'Сообщений нет',
            style: TextStyle(fontSize: 20, color: Colors.black38),
          ));
        } else {
          return Scrollbar(
            child: ListView.builder(
              padding: EdgeInsets.all(0),
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) =>
                  ChatContentItem(snapshot.data[index], _bloc),
            ),
            // ),
          );
        }
      },
    );

    var actions = <Widget>[];
    if (_bloc.unreadChatIds.isNotEmpty) {
      actions.add(Padding(
        padding: const EdgeInsets.all(8),
        child: Stack(
          alignment: AlignmentDirectional(0.8, -0.8),
          children: [
            IconButton(
                icon: Icon(Icons.notifications),
                tooltip: 'New messages',
                color: Colors.white,
                onPressed: () {
                  _bloc.onTapNewMessageNotification();
                }),
            CircleAvatar(
              radius: 8.0,
              backgroundColor: Colors.red,
              child: Text('${_bloc.unreadChatIds.length}',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                      fontWeight: FontWeight.bold)),
            ),
          ],
        ),
      ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(chatMemberNames()),
        actions: actions,
      ),
      body: Container(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0, bottom: 8.0),
        child: Column(
          children: <Widget>[
            Expanded(child: messagesList),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _sendMessageTextController,
                    decoration: InputDecoration(hintText: 'Your message'),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.send),
                  onPressed: () {
                    var text = _sendMessageTextController.text;
                    if (text.isNotEmpty) {
                      _bloc.sendMessage(text);
                      _sendMessageTextController.clear();
                    }
                  },
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  String chatMemberNames() {
    return _bloc.currentChat.members
        .where((user) => user.id != _bloc.currentUser.id)
        .map((user) => user.name)
        .join(", ");
  }
}
