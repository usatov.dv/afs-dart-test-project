import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';

import '../../bloc_common/i_bloc.dart';
import '../../data/repository.dart';
import '../../di/screen_builder.dart';
import '../../entities/app_state.dart';

class StartupBloc extends IBloc {
  StartupBloc(
      {@required AppState appState,
      @required Repository repository,
      @required ScreenBuilder screenBuilder})
      : _appState = appState,
        _repository = repository,
        _screenBuilder = screenBuilder {
    isInitComplete = initialize();
    _log.i('create');
  }

  final AppState _appState;
  final Repository _repository;
  final ScreenBuilder _screenBuilder;
  Future<bool> isInitComplete;
  BuildContext context;
  final FimberLog _log = FimberLog('StartupBloc');

  Future<bool> initialize() async {
    _log.d('initialize() start');

    await _repository.isInitComplete;

    _log.d('initialize() end');
    return true;
  }

  void gotoNextScreen() {
    Widget Function() nextScreen;
    if (_appState.currentUser == null) {
      nextScreen = _screenBuilder.getLoginScreenBuilder();
    } else {
      nextScreen = _screenBuilder.getChatListScreenBuilder();
    }
    Navigator.pushReplacement<Widget, Widget>(
        context,
        MaterialPageRoute<Widget>(
            builder: (BuildContext context) => nextScreen()));
  }

  @override
  void dispose() {
    _log.i('dispose');
  }
}
