import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

import 'chat_list_bloc.dart';

class ChatListItem extends StatelessWidget {
  ChatListItem(this.item, this.bloc);

  final Chat item;
  final ChatListBloc bloc;

  @override
  Widget build(BuildContext context) {
    final backColor = avatarColor(context);
    final textColor =
        backColor.computeLuminance() > 0.5 ? Colors.black : Colors.white;

    final _avatar = Padding(
        padding: EdgeInsets.all(8.0),
        child: CircleAvatar(
          radius: 32.0,
          backgroundColor: backColor,
          foregroundColor: textColor,
          child: Text(getInitials(), style: TextStyle(fontSize: 20.0)),
        ));

    final _members = Text.rich(TextSpan(children: chatMemberNames()),
        overflow: TextOverflow.ellipsis);

    final _lastMessage =
        Text('Последнее сообщение', overflow: TextOverflow.ellipsis);

    final _lastMessageTime = Text('Дата');

    final _textDivider = SizedBox(height: 8.0);

    return Container(
      height: 80.0,
      child: InkWell(
        highlightColor: Theme.of(context).primaryColorLight,
        splashColor: Theme.of(context).primaryColorLight,
        onTap: () {
          bloc.onTapChatItem(context, item);
        },
        child: Row(
          children: <Widget>[
            _avatar,
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(color: Colors.black26, width: 1))),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[_members, _textDivider, _lastMessage],
                  ),
                ),
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.black26, width: 1))),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _lastMessageTime,
                    _textDivider,
                    _unreadMessageCount(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getInitials() {
    var result = '?';
    var members = <User>[];
    members.addAll(item.members);
    members.removeWhere((user) => user.id == bloc.currentUser.id);
    var name = members[0].name;
    if (name != null) {
      var split = name.split(' ');
      result = split[0].substring(0, 1).toUpperCase();
      if (split.length > 1) {
        result = result + split[1].substring(0, 1).toUpperCase();
      }
    }
    return result;
  }

  List<InlineSpan> chatMemberNames() {
    var result = <InlineSpan>[];
    if (item.members != null) {
      var other =
          item.members.where((user) => user.id != bloc.currentUser.id).toList();
      for (var i = 0; i < other.length; i++) {
        if (bloc.isCheckedUser(other[i])) {
          result.add(TextSpan(
            text: ' ${other[i].name} ',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.white,
                backgroundColor: Colors.purple),
          ));
        } else {
          result.add(TextSpan(
              text: other[i].name,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)));
        }
        if (i < other.length - 1) {
          result.add(TextSpan(
              text: ', ',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0)));
        }
      }
    }
    return result;
  }

  Widget _unreadMessageCount() {
    if (bloc.isUnreadChat(item)) {
      return Container(
          constraints: BoxConstraints(minWidth: 24.0),
          decoration: BoxDecoration(
              color: Colors.green,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
          padding: EdgeInsets.all(4.0),
          alignment: Alignment.center,
          child: Text('?', style: TextStyle(color: Colors.white)));
    } else {
      return SizedBox(height: 24.0);
    }
  }

  Color avatarColor(BuildContext context) {
    var length = Colors.primaries.length;
    var index = getInitials().codeUnits[0] % length;
    return Colors.primaries[index][300];
  }
}
