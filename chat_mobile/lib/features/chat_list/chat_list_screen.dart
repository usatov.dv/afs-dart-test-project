import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../../bloc_common/bloc_provider.dart';
import 'chat_list_bloc.dart';
import 'chat_list_item.dart';
import 'user_list_item.dart';

class ChatListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChatListScreenState();
}

class _ChatListScreenState extends State<ChatListScreen> {
  ChatListBloc _bloc;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final ItemScrollController _chatScrollController = ItemScrollController();
  final ItemPositionsListener _chatPositionsListener =
      ItemPositionsListener.create();
  final ItemScrollController _userScrollController = ItemScrollController();
  final ItemPositionsListener _userPositionsListener =
      ItemPositionsListener.create();

  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of(context);
    _bloc.context = context;
    _bloc.needRefreshUI.stream.listen((event) {
      setState(() {});
    });
    // saver scroll position for user list
    _userPositionsListener.itemPositions.addListener(() {
      var positions = _userPositionsListener.itemPositions.value;
      if (positions.isNotEmpty) {
        // min full-visible item
        _bloc.tabUsersScrollIndex = positions
            .where((position) => position.itemLeadingEdge >= 0)
            .reduce((min, position) =>
                position.itemTrailingEdge < min.itemTrailingEdge
                    ? position
                    : min)
            .index;
      }
    });
    // saver scroll position for chat list
    _chatPositionsListener.itemPositions.addListener(() {
      var positions = _chatPositionsListener.itemPositions.value;
      if (positions.isNotEmpty) {
        // min full-visible item
        _bloc.tabChatsScrollIndex = positions
            .where((position) => position.itemLeadingEdge >= 0)
            .reduce((min, position) =>
                position.itemTrailingEdge < min.itemTrailingEdge
                    ? position
                    : min)
            .index;
      }
    });
    // show notification about unread messages
    _bloc.notificationStream.listen((text) async {
      var overlayState = Overlay.of(context);
      var overlayEntry = OverlayEntry(
          builder: (context) => Align(
              alignment: Alignment(0.0, -0.9),
              child: Container(
                height: 48,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.blue[100],
                    borderRadius: BorderRadius.all(Radius.circular(24))),
                child: Center(
                  child: Text(text,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto',
                          decoration: TextDecoration.none)),
                ),
              )));
      overlayState.insert(overlayEntry);
      await Future.delayed(Duration(seconds: 5));
      overlayEntry.remove();
    });
  }

  @override
  Widget build(BuildContext context) {
    final tabs = <Tab>[
      Tab(child: Text('Пользователи', overflow: TextOverflow.ellipsis)),
      Tab(child: Text('Чаты', overflow: TextOverflow.ellipsis)),
    ];

    final tabBar = TabBar(
      tabs: tabs,
      onTap: (int index) {
        setState(() {
          _bloc.onTapTabBar(index);
        });
      },
    );

    final usersList = StreamBuilder(
      stream: _bloc.usersStream,
      builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
        if (snapshot.data == null) {
          return Center(
              child: CircularProgressIndicator(
            strokeWidth: 2.0,
          ));
        } else if (snapshot.data.isEmpty) {
          return Center(
              child: Text(
            'Пользователей нет',
            style: TextStyle(fontSize: 20, color: Colors.black38),
          ));
        } else {
          return RefreshIndicator(
            onRefresh: _bloc.refreshUsers,
            child: Scrollbar(
              child: ScrollablePositionedList.builder(
                padding: EdgeInsets.all(0),
                itemCount: snapshot.data.length,
                itemScrollController: _userScrollController,
                itemPositionsListener: _userPositionsListener,
                initialScrollIndex: _bloc.tabUsersScrollIndex,
                itemBuilder: (BuildContext context, int index) =>
                    UserListItem(snapshot.data[index], _bloc),
              ),
            ),
          );
        }
      },
    );

    final chatsList = StreamBuilder(
      stream: _bloc.chatsStream,
      builder: (BuildContext context, AsyncSnapshot<List<Chat>> snapshot) {
        if (snapshot.data == null) {
          return Center(
              child: CircularProgressIndicator(
            strokeWidth: 2.0,
          ));
        } else if (snapshot.data.isEmpty) {
          return Center(
              child: Text(
            'Чатов нет',
            style: TextStyle(fontSize: 20, color: Colors.black38),
          ));
        } else {
          return RefreshIndicator(
            onRefresh: _bloc.refreshChats,
            child: Scrollbar(
              child: ScrollablePositionedList.builder(
                padding: EdgeInsets.all(0),
                itemCount: snapshot.data.length,
                itemScrollController: _chatScrollController,
                itemPositionsListener: _chatPositionsListener,
                initialScrollIndex: _bloc.tabChatsScrollIndex,
                itemBuilder: (BuildContext context, int index) =>
                    ChatListItem(snapshot.data[index], _bloc),
              ),
            ),
          );
        }
      },
    );

    final drawer = Drawer(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text('Тут будет меню'),
            SizedBox(
              height: 30,
            ),
            MaterialButton(
              onPressed: _closeDrawer,
              child: const Text(
                'Понятно',
                style: TextStyle(color: Colors.white),
              ),
              color: Theme.of(context).primaryColor,
            ),
          ],
        ),
      ),
    );

    return DefaultTabController(
        length: tabs.length,
        initialIndex: _bloc.currentTabBarIndex,
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text('Simple Chat'),
              bottom: tabBar,
            ),
            drawer: drawer,
            body: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              children: [usersList, chatsList],
            ),
            floatingActionButton: (_bloc.checkedUserIds.isNotEmpty &&
                    _bloc.currentTabBarIndex == 0)
                ? FloatingActionButton(
                    child: Icon(Icons.add), onPressed: _bloc.addNewChat)
                : SizedBox.shrink()));
  }

  void _openDrawer() {
    _scaffoldKey.currentState.openDrawer();
  }

  void _closeDrawer() {
    Navigator.of(context).pop();
  }
}
