import 'dart:async';
import 'dart:collection';

import 'package:chat_models/chat_models.dart';
import 'package:fimber/fimber.dart';
import 'package:flutter/material.dart';

import '../../bloc_common/i_bloc.dart';
import '../../data/repository.dart';
import '../../di/screen_builder.dart';
import '../../entities/app_state.dart';

class ChatListBloc extends IBloc {
  ChatListBloc(
      {@required AppState appState,
      @required Repository repository,
      @required ScreenBuilder screenBuilder})
      : _appState = appState,
        _repository = repository,
        _screenBuilder = screenBuilder {
    initialize();
    _log.i('create');
  }

  final AppState _appState;
  final Repository _repository;
  final ScreenBuilder _screenBuilder;
  Stream<List<Chat>> chatsStream;
  Stream<List<User>> usersStream;
  Stream<String> notificationStream;
  Stream<Set<ChatId>> unreadChatIdsStream;
  Set<ChatId> _unreadChatIds = HashSet<ChatId>();
  User currentUser;
  int currentTabBarIndex;
  int tabUsersScrollIndex;
  int tabChatsScrollIndex;
  List<UserId> checkedUserIds = [];
  StreamController<bool> needRefreshUI = StreamController<bool>();
  BuildContext context;
  final _log = FimberLog('ChatListBloc');

  void initialize() {
    chatsStream = _repository.chatsStream;
    usersStream = _repository.usersStream;
    notificationStream = _repository.notificationStream;
    unreadChatIdsStream = _repository.unreadChatIdsStream;
    unreadChatIdsStream.listen((unreadChatIds) {
      _unreadChatIds.clear();
      _unreadChatIds.addAll(unreadChatIds);
    });
    refreshChats();
    refreshUsers();
    currentUser = _appState.currentUser;
    currentTabBarIndex = _appState.tabBarIndex ?? 0;
    tabUsersScrollIndex = _appState.tabUsersScrollIndex ?? 0;
    tabChatsScrollIndex = _appState.tabChatsScrollIndex ?? 0;
  }

  Future<void> refreshUsers() async {
    await _repository.getUsers();
    checkedUserIds.clear();
    needRefreshUI.add(true);
  }

  Future<void> refreshChats() async {
    await _repository.getChats();
    needRefreshUI.add(true);
  }

  Future<void> addNewChat() async {
    var newChat = await _repository.createChat(memberIds: checkedUserIds);
    if (newChat != null) {
      goToChatContent(chat: newChat);
    }
  }

  bool isUnreadChat(Chat chat) {
    return _unreadChatIds.contains(chat.id);
  }

  bool isCheckedUser(User user) {
    return checkedUserIds.contains(user.id);
  }

  void onTapTabBar(int index) {
    if (index != currentTabBarIndex) {
      currentTabBarIndex = index;
      if (index == 0) {
        // _repository.getUsers();
      } else {
        // _repository.getChats();
      }
    }
  }

  void onTapChatItem(BuildContext context, Chat chat) {
    goToChatContent(chat: chat);
  }

  void onTapUserItem(BuildContext context, User user) {
    if (checkedUserIds.contains(user.id)) {
      checkedUserIds.remove(user.id);
    } else {
      checkedUserIds.add(user.id);
    }
    needRefreshUI.add(true);
  }

  void goToChatContent({@required Chat chat}) {
    _appState.currentChat = chat;
    checkedUserIds.clear();
    // needRefreshUI.add(true);
    var nextScreen = _screenBuilder.getChatContentScreenBuilder();
    Navigator.push(
        context, MaterialPageRoute<Widget>(builder: (context) => nextScreen()));
  }

  @override
  void dispose() {
    _appState.tabBarIndex = currentTabBarIndex;
    _appState.tabUsersScrollIndex = tabUsersScrollIndex;
    _appState.tabChatsScrollIndex = tabChatsScrollIndex;
    _log.i('dispose');
  }
}
