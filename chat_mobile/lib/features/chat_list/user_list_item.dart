import 'package:chat_models/chat_models.dart';
import 'package:flutter/material.dart';

import 'chat_list_bloc.dart';

class UserListItem extends StatelessWidget {
  UserListItem(this.item, this.bloc);

  final User item;
  final ChatListBloc bloc;

  @override
  Widget build(BuildContext context) {
    final backColor = avatarColor(context);
    final textColor =
        backColor.computeLuminance() > 0.5 ? Colors.black : Colors.white;

    final _avatar = Stack(
      alignment: AlignmentDirectional(0.8, 0.8),
      children: [
        Padding(
            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
            child: CircleAvatar(
              radius: 24.0,
              backgroundColor: backColor,
              foregroundColor: textColor,
              child: Text(getInitials(), style: TextStyle(fontSize: 20.0)),
            )),
        if (bloc.isCheckedUser(item))
          CircleAvatar(
            radius: 12.0,
            backgroundColor: Colors.white,
            child: CircleAvatar(
                radius: 10.0,
                backgroundColor: Colors.green,
                child: Icon(
                  Icons.check,
                  size: 16,
                  color: Colors.white,
                )),
          ),
      ],
    );

    final _firstString = Text(
      item.name,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),
      overflow: TextOverflow.ellipsis,
    );

    final _secondString =
        Text(item.email ?? 'no e-mail', overflow: TextOverflow.ellipsis);

    final _textDivider = SizedBox(height: 8.0);

    return Container(
      height: 68.0,
      child: InkWell(
        highlightColor: Theme.of(context).primaryColorLight,
        splashColor: Theme.of(context).primaryColorLight,
        onTap: () {
          bloc.onTapUserItem(context, item);
        },
        child: Row(
          children: <Widget>[
            _avatar,
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[_firstString, _textDivider, _secondString],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getInitials() {
    var result = '?';
    var name = item.name;
    if (name != null) {
      var split = name.split(' ');
      result = split[0].substring(0, 1).toUpperCase();
      if (split.length > 1) {
        result = result + split[1].substring(0, 1).toUpperCase();
      }
    }
    return result;
  }

  Color avatarColor(BuildContext context) {
    var length = Colors.primaries.length;
    var index = getInitials().codeUnits[0] % length;
    return Colors.primaries[index][300];
  }
}
