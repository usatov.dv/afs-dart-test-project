import 'dart:convert';

import 'package:chat_models/chat_models.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test("_handleDataFromProvider()", () {
    var data = 'asdf: asdfasdf , df:werwe';
    var receivedMessage;
    var chatId;
    try {
      receivedMessage = Message.fromJson(json.decode(data));
    } on Exception catch (err) {
      print('err = $err');
    }
    if (receivedMessage != null) {
      chatId = receivedMessage.chat;
    }

    expect(chatId, null);
  });
}
